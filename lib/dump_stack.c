// SPDX-License-Identifier: GPL-2.0
/*
 * Provide a default dump_stack() function for architectures
 * which don't implement their own.
 */

#include <linux/kernel.h>
#include <linux/export.h>
#include <linux/sched.h>
#include <linux/sched/debug.h>
#include <linux/smp.h>
#include <linux/atomic.h>
#include <linux/kexec.h>
#include <linux/utsname.h>

/* Start: ICMC edits by Muhammad Laghari (mlaghari@vt.edu) */
#define CDAT_SIZE 5

extern unsigned long long *ml_kmalloc;
extern unsigned long long Partition_ID[CDAT_SIZE];

unsigned long long query(int command, int partID) {
  unsigned long long toReturn = 0;
  int part_ID = 0; //*temp_ml_kmalloc = NULL;
  unsigned long long *p = (unsigned long long *) ml_kmalloc;
  part_ID = partID;
  if (command == 0) {
    p += 16/8;
    toReturn = *p & 0xffffff;
  } else if (command == 1) {
    p += 24/8;
    toReturn = *p & 0xffffff;
  } else if (command == 2) {
    p += (32/8 + part_ID);
    toReturn = *p & 0xffffffffffff;
  } else if (command == 3) {
    p += (64/8 + part_ID);
    toReturn = *p & 0xffffff;
  } else if (command == 4) {
    p += (96/8 + part_ID);
    toReturn = *p & 0xffffff;
  } else if (command == 5) {
    p += (128/8 + part_ID);
    toReturn = *p & 0xffffff;
  } else if (command == 6) {
    p += (160/8 + part_ID);
    toReturn = *p & 0xffffff;
  }
  return toReturn;
}

void printICMCState(void) {
  unsigned long long query_output[7];
  int i = 0;
  query_output[0] = query(0, -1); // Global Free List
  query_output[1] = query(1, -1); // ZSPage List Size
  printk(KERN_EMERG "[ICMC] ================ICMC Hardware Counters=====================");
  for (i = 0 ; i < CDAT_SIZE-1 ; i++) {
    query_output[2] = query(2, i); // Usage
    query_output[3] = query(3, i); // # of Raw pages
    query_output[4] = query(4, i); // # of compressed pages
    query_output[5] = query(5, i); // # of incompressible pages
    query_output[6] = query(6, i); // Expansion count
    if (i == 0)
      printk(KERN_EMERG "[ICMC] [Default Mem Usage Controller] Actual Usage: %lld MB, OS Physical Usage: %lld MB {Uncompr. Mem: %lld MB, Compr. Mem: %lld MB}", query_output[2]/1024/1024, (query_output[3] + query_output[4] + query_output[5])*4/1024, (query_output[3]+query_output[5])*4/1024, query_output[4]*4/1024);
    else
      printk(KERN_EMERG "[ICMC] [Mem Usage Controller: %d] Watchpoint: %llu MB, Actual Usage: %lld MB, OS Physical Usage: %lld MB {Uncompr. Mem: %lld MB, Compr. Mem: %lld MB}, Expansions: %llu", i, Partition_ID[i], query_output[2]/1024/1024, (query_output[3] + query_output[4] + query_output[5])*4/1024, (query_output[3]+query_output[5])*4/1024, query_output[4]*4/1024, query_output[6]);
  }
  printk(KERN_EMERG "[ICMC] ===========================================================");
}

/* End: ICMC edits by Muhammad Laghari (mlaghari@vt.edu) */
static char dump_stack_arch_desc_str[128];

/**
 * dump_stack_set_arch_desc - set arch-specific str to show with task dumps
 * @fmt: printf-style format string
 * @...: arguments for the format string
 *
 * The configured string will be printed right after utsname during task
 * dumps.  Usually used to add arch-specific system identifiers.  If an
 * arch wants to make use of such an ID string, it should initialize this
 * as soon as possible during boot.
 */
void __init dump_stack_set_arch_desc(const char *fmt, ...)
{
	va_list args;

	va_start(args, fmt);
	vsnprintf(dump_stack_arch_desc_str, sizeof(dump_stack_arch_desc_str),
		  fmt, args);
	va_end(args);
}

/**
 * dump_stack_print_info - print generic debug info for dump_stack()
 * @log_lvl: log level
 *
 * Arch-specific dump_stack() implementations can use this function to
 * print out the same debug information as the generic dump_stack().
 */
void dump_stack_print_info(const char *log_lvl)
{
	printk("%sCPU: %d PID: %d Comm: %.20s %s%s %s %.*s\n",
	       log_lvl, raw_smp_processor_id(), current->pid, current->comm,
	       kexec_crash_loaded() ? "Kdump: loaded " : "",
	       print_tainted(),
	       init_utsname()->release,
	       (int)strcspn(init_utsname()->version, " "),
	       init_utsname()->version);

	if (dump_stack_arch_desc_str[0] != '\0')
		printk("%sHardware name: %s\n",
		       log_lvl, dump_stack_arch_desc_str);

	print_worker_info(log_lvl, current);
}

/**
 * show_regs_print_info - print generic debug info for show_regs()
 * @log_lvl: log level
 *
 * show_regs() implementations can use this function to print out generic
 * debug information.
 */
void show_regs_print_info(const char *log_lvl)
{
	dump_stack_print_info(log_lvl);
}

static void __dump_stack(void)
{
	dump_stack_print_info(KERN_DEFAULT);
	show_stack(NULL, NULL);
}

/**
 * dump_stack - dump the current task information and its stack trace
 *
 * Architectures can override this implementation by implementing its own.
 */
#ifdef CONFIG_SMP
static atomic_t dump_lock = ATOMIC_INIT(-1);

asmlinkage __visible void dump_stack(void)
{
	unsigned long flags;
	int was_locked;
	int old;
	int cpu;

	/*
	 * Permit this cpu to perform nested stack dumps while serialising
	 * against other CPUs
	 */
retry:
	local_irq_save(flags);
	cpu = smp_processor_id();
	old = atomic_cmpxchg(&dump_lock, -1, cpu);
	if (old == -1) {
		was_locked = 0;
	} else if (old == cpu) {
		was_locked = 1;
	} else {
		local_irq_restore(flags);
		cpu_relax();
		goto retry;
	}

	__dump_stack();

	if (!was_locked)
		atomic_set(&dump_lock, -1);

	local_irq_restore(flags);
}
#else
asmlinkage __visible void dump_stack(void)
{
  unsigned long long temp_value = 0;
  if (ml_kmalloc != NULL) {
    temp_value |= 0x0e00000000000000;
    *ml_kmalloc = temp_value;
    temp_value = 0;
    printICMCState();
  }
  __dump_stack();
}
#endif
EXPORT_SYMBOL(dump_stack);
